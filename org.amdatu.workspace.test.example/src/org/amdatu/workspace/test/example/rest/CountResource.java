package org.amdatu.workspace.test.example.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.amdatu.web.rest.doc.Description;
import org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants;
import org.amdatu.workspace.test.example.CountService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Path("/count")
@Component(provides = Object.class, properties =
    @Property(name = AmdatuWebRestConstants.JAX_RS_RESOURCE_BASE, value = "/")
)
public class CountResource {

    @ServiceDependency
    private volatile CountService m_countService;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Description("Get the current count from the CountService")
    public String demo() {
        return "The count is: " + m_countService.getCount();
    }
}
