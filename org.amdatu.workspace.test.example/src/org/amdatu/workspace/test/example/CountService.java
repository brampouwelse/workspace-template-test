package org.amdatu.workspace.test.example;

public interface CountService {

    Long getCount();

}
