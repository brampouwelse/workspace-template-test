package org.amdatu.workspace.test.example.scheduling;

import java.util.concurrent.atomic.AtomicLong;

import org.amdatu.scheduling.Job;
import org.amdatu.scheduling.annotations.RepeatForever;
import org.amdatu.scheduling.annotations.RepeatInterval;
import org.amdatu.workspace.test.example.CountService;
import org.apache.felix.dm.annotation.api.Component;

@Component
@RepeatForever
@RepeatInterval(period= RepeatInterval.SECOND, value = 5)
public class JobCountService implements Job, CountService {

    private AtomicLong m_count = new AtomicLong();


    @Override
    public void execute() {
        m_count.incrementAndGet();
    }

    public Long getCount() {
        return m_count.get();
    };
}
