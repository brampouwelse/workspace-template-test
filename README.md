# Quick demo of some ideas for a new Amdatu workspace template

  - The workspace contains a curated repo that contains bundles that are known to work together
  
  For this quick test we used a local repo where the bundles are contained in the template itself eventually this should be replaced by a JPM / Maven repo as we then only need to include pointers to bundles instead of the actual bundle.
  
  - To get up and running quickly there there are default `-buildpath` entries in managed from `cnf/ext/*.bnd` for:
    
    - osgi (core / cmpn/ annotation)
    - dependencymanager 
    - amdatu-web
    - amdatu-scheduling
    
    
  - To get the application running quickly there is a template that contains all the basics which can be included by adding `-include: ${workspace}/cnf/run-templates/amdatu.bndrun` to a bndrun file
  
  
## Try it out: 
  
  - Create empty eclipse workspace
  - Open Eclipse preferences -> Bndtools -> Workspace Template
  - Hit + sign next to the 'Raw git clone url section'
  - Use clone url: `https://bitbucket.org/brampouwelse/workspace-template-test.git` 
  
  - Save, ok ... 
  
  - New Other... (cmd + N)
  - Select Bndtools -> Bnd OSGi workspace
  - Select the git repo that was added in the previous step
  - ok ok ok next ok finish
